package main.java.nl.hu.hadoop.friendlynumbers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;

public class FriendlyNumbers {

	public static void main(String[] args) throws Exception {
		Job job = new Job();
		job.setJarByClass(FriendlyNumbers.class);

		FileInputFormat.addInputPath(job, new Path("tcifbigdata/code/FriendlyNumbers/input"));
		FileOutputFormat.setOutputPath(job, new Path("tcifbigdata/code/FriendlyNumbers/output"));

		job.setMapperClass(FriendlyNumbersMapper.class);
		//job.setCombinerClass(FriendlyNumbersCombiner.class);
		job.setReducerClass(FriendlyNumbersReducer.class);
		//job.setInputFormatClass(TextInputFormat.class);
		//job.setOutputKeyClass(Text.class);
		//job.setOutputValueClass(IntWritable.class);

		job.waitForCompletion(true);
	}
}

//Achterhaal welke delers elk getal heeft
//(10,2)
//(10,5)
class FriendlyNumbersMapper extends Mapper<LongWritable, Text, Text, Text> {

	private int number;
	private int sumDevisors;
	private ArrayList<Integer> indentityMap = new ArrayList<>();
	
	public void map(LongWritable Key, Text value, Context context) throws IOException, InterruptedException {
		String[] tokens = value.toString().split("\\s");
		for (String s : tokens) {
			number = Integer.parseInt(s);
			if(!indentityMap.contains(number)){
				sumDevisors = sumFactors(number);
				if(sumFactors(sumDevisors) == number){
					context.write(new Text("Found"), new Text(number + " " + sumDevisors));
				}
			}
		}
	}
	
	public int sumFactors(int number){
		int sum = 0;
		for(int i = 1; i < number; i++){
			if(number % i == 0){
				sum += i;
			}
		}
		return sum;
	}
}

//combineer delers per key (10, [2,5])
/*class FriendlyNumbersCombiner extends Reducer<Text, IntWritable, Text, ArrayList<IntWritable>>{
	public void combine(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
		
		ArrayList<IntWritable> devisors = new ArrayList<IntWritable>();
		HashMap<String, ArrayList<IntWritable>> combinations = new HashMap<String, ArrayList<IntWritable>>();
		for (IntWritable i : values){
			devisors.add(i);
		}
		combinations.put(key.toString(), devisors);
		context.write(key, devisors);
	}
}*/


class FriendlyNumbersReducer extends Reducer<Text, Text, Text, Text> {
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		String output = "";
		for (Text t : values) {
			output += t.toString();
		}	
		HashMap<Integer, Integer> result = new HashMap<>();
		
		Scanner scanner = new Scanner(output);
		scanner.useDelimiter("");
		
		while(scanner.hasNext()){
			result.put(scanner.nextInt(), scanner.nextInt());
		}
		
		scanner.close();
		
		for(Integer number : result.keySet()){
			int current = result.get(number);
			for (Integer i : result.keySet()){
				if(current == i && i != number){
					context.write(new Text("Found: "), new Text(i + " and " + number));
				}
			}
		}
	}
}

